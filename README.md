# Projet agile de Septembre IUT de Lille

## Objectifs

* Remettre les étudiants dans le bain de la programmation juste après les vacances d'été.
* Remettre les étudiants dans le bain de la gestion de code source en équipe.
* Créer de la cohésion dans la promo.
* Faire une très courte découverte de l'agilité.

## SAÉ 3.1

* Suivi de projet en fonction des besoins métier, compétences 5 de la SAÉ
    * PDCA.
    * suivi rigoureux de la méthode.

* Rôle et mission au sein d'une équipe informatique
    * note individuelle
    * niveau d'implication dans le projet
    * list des commit git ?
    * usage des issues gitlab ?

* Resource R3.10
    * QCM sur le vocabulaire agile
    * mise en situation

### Apprentissages critiques

- AC21.01 : Élaborer et implémenter les spécifications fonctionnelles et non fonctionnelles à partir des exigences
- AC21.02 : Appliquer des principes d’accessibilité et d’ergonomie
- AC21.03 : Adopter de bonnes pratiques de conception et de programmation
- AC21.04 : Vérifier et valider la qualité de l’application par les tests
- AC22.01 : Choisir des structures de données complexes adaptées au problème
- AC23.01 : Concevoir et développer des applications communicantes
- AC23.02 : Utiliser des serveurs et des services réseaux virtualisés
- AC24.03 : Organiser la restitution de données à travers la programmation et la visualisation
- AC24.04 : Manipuler des données hétérogènes
- AC25.02 : Formaliser les besoins du client et de l'utilisateur
- AC25.03 : Identifier les critères de faisabilité d’un projet informatique
- AC25.04 : Définir et mettre en œuvre une démarche de suivi de projet
- AC26.02 : Appliquer une démarche pour intégrer une équipe informatique au sein d’une organisation
- AC26.03 : Mobiliser les compétences interpersonnelles pour travailler dans une équipe informatique
- AC26.04 : Rendre compte de son activité professionnelle

## Déroulé

L'activité se déroulant sur 3,5j

* un amphi d'introduction d'une heure environ
* une session de découverte (2h) de l'agilité via un jeu sérieux Lego4Scrum [les notes pour les animateurs](https://gitlab.com/tclavier/lego4scrum/builds/artifacts/master/file/notes_pour_le_facilitateur.pdf?job=build_pdf)
* 5 demi-journées structurées en sprint de 2h
  * 1h30 de développement
  * 10mn de démo
  * 10mn de rétrospective
* une demi-journée de conclusion
  * 2h de présentation aux premières années (10 min par salles)
  * 1h de QCM sur l'agilité
  * 1h de rétrospective sur l'activité (salle de DS)

* Présentation générale des expérimentations menées autour de l'initiation à la démarche agile au département
  * https://github.com/tclavier/rex-iut-agile-laval
  * https://github.com/tclavier/rex-iut-agile-laval/blob/master/CLAVIER_SECQ_AgileLaval16.pdf
* Support d'introduction de l'amphi
  * https://gitlab.com/tclavier/intro-projet-agile-iut
* Synopsis Lego4Scrum
  * https://gitlab.com/tclavier/lego4scrum

## Notes pour les intervenants

Nous avons décidé collectivement les années précédentes de faire l'appel, le documents google partagé permet de le faire facilement.

[Support de mars pour les intervenats](projet-de-mars/support-intervenants.md) on y trouve entre autre quelques pistes pour animer les rétrospectives.
